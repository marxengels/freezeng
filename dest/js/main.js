$(document).ready(function() {
  checkEnableTabs();
  $(window).resize(function() {checkEnableTabs()});

  $elem = $('.stages__content__item[aria-hidden=false] .stages__content__image img');
  if ($elem.length) {
    $elem.hide();
    $(window).on('scroll', function() {
      if (isScrolledIntoView($elem.closest('.stages__content__item'))) $elem.fadeIn();
    });
  }

  $( "#params" ).tabs({
    hide: { effect: "slide", duration: 300, direction: "left" },
    show: { effect: "slide", duration: 300, direction: "right" }
  });

  $('.object__gallery-slider').slick({
    slidesToShow: 1,
    dots:         true,
    arrows:       false
  });

  var slideout = new Slideout({
    'panel': document.getElementById('panel'),
    'menu':  document.getElementById('menu'),
    'side':  'left'
  });
  document.getElementById('mobile-menu-btn').addEventListener('click', function() {
    slideout.toggle();
  });

  $('body').fadeIn('500');
  $('a').click(function(e) {
    var href               = this.href;
    var goToWithoutHash    = href.split('#')[0];
    var currentWithoutHash = window.location.href.split('#')[0];
    if ((goToWithoutHash == currentWithoutHash) || !href) return true;
    e.preventDefault();
    $('body').fadeOut('500', function() {
      window.location = href;
      $('body').fadeIn('500');
    });
  });

  var $serviceGrid = $('.service-animated').isotope({
    itemSelector: '.service__item__wrapper',
    layoutMode: 'fitRows'
  });
  $('.service-animated-link').click(function(e) {
    $('.services__menu__link').removeClass('active');
    $(this).addClass('active');
    var selectedType = $(this).data('href');
    selectedType = selectedType == '*' ? selectedType : '.' + selectedType;
    $serviceGrid.isotope({filter: selectedType});
  });

  increaseNumber('#month',     0, 18, 1, 300);
  increaseNumber('#watt',      0, 1.5, 0.1, 200);
  increaseNumber('#furniture', 0, 60, 1, 70);

  increaseNumber('#year',     0, 20, 1, 150);
  increaseNumber('#employee', 0, 60, 2, 75);
  increaseNumber('#objects',  0, 1500, 5, 1);
});

function initMap() {
  var uluru = {lat: -25.363, lng: 131.044};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 4,
    center: uluru
  });
  var marker = new google.maps.Marker({
    position: uluru,
    map: map
  });
}

function checkEnableTabs() {
  if ($(window).width() > '500') {
    $( ".stages" ).tabs({
      hide: { effect: "slide", duration: 300, direction: "left" },
      show: { effect: "slide", duration: 300, direction: "right" }
    });
    $( ".stages" ).tabs( "enable" );
  } else {
    $( ".stages" ).tabs( "disable" );
  }
}

function isScrolledIntoView(elem) {
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();

  var elemTop = $(elem).offset().top;
  var elemBottom = elemTop + $(elem).height();

  return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

function increaseNumber(selector, from, to, step, timeout) {
  $(selector).text(from);

  var new_from = Math.round(Math.min(from + step, to) * 10) / 10;

  if (new_from < to) {
    setTimeout(function() {
      increaseNumber(selector, new_from, to, step, timeout)
    }, timeout);
  }
  
  if (new_from == to) {
    setTimeout(function() {
      $(selector).text(to);
    }, timeout);
  }
}