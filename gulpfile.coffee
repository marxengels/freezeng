gulp          = require 'gulp'
plumber       = require 'gulp-plumber'
sass          = require 'gulp-sass'
notify        = require 'gulp-notify'
imagemin      = require 'gulp-imagemin'
autoprefixer  = require 'gulp-autoprefixer'
pngquant      = require 'imagemin-pngquant'
browserSync   = require 'browser-sync'
slim          = require 'gulp-slim'

reload = browserSync.reload

sync =
  css: 'stylesheets/**/*.css'
  html: '**/*.html'

srcs =
  scss: [
    'source/scss/**/*.scss',
    '!source/scss/**/_*.scss'
  ]
  img:  'source/images/*'
  slim: [
    'source/views/**/*.slim',
    '!source/views/**/_*.slim'
  ]

dests =
  css:  'dest/css'
  img:  'dest/images'
  html: 'dest'

gulp.task 'browserSync', ->
  browserSync({
    startPath:'./',
    server: {
      baseDir: "./dest"
    },
    port:   8080,
    open:   true,
    notify: true
  });

gulp.task 'watch', ->
  gulp.watch srcs.scss,   ['css']
  gulp.watch srcs.images, ['media']
  gulp.watch srcs.slim,   ['slim']

gulp.task 'css', ->
  gulp.src srcs.scss
    .pipe(plumber())
    .pipe sass()
    .pipe autoprefixer(cascade: false, browsers: ['> 1%'])
    .pipe gulp.dest(dests.css)
    .pipe notify("File <%= file.relative %> was successfully compiled!")
    .pipe reload({ stream: true })

gulp.task 'media', ->
  gulp.src srcs.img
    .pipe imagemin({
      progressive: true,
      use: [pngquant()]
    })
    .pipe gulp.dest dests.img

gulp.task 'slim', ->
  gulp.src srcs.slim
    .pipe(plumber())
    .pipe slim()
    .pipe gulp.dest(dests.html)
    .pipe notify("File <%= file.relative %> was successfully compiled!")
    .pipe reload({ stream: true })


gulp.task 'default', ['media', 'css', 'slim', 'watch', 'browserSync']